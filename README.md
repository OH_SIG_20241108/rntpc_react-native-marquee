# @react-native-ohos/react-native-marquee

This project is based on [react-native-marquee](https://github.com/kyo504/react-native-marquee)

## Documentation

- [中文](https://gitee.com/react-native-oh-library/usage-docs/blob/master/zh-cn/react-native-marquee.md)

- [English](https://gitee.com/react-native-oh-library/usage-docs/blob/master/en/react-native-marquee.md)

## License

This library is licensed under [The MIT License (MIT)](https://gitee.com/openharmony-sig/rntpc_react-native-marquee/blob/master/LICENSE)