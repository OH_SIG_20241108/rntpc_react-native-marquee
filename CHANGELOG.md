# v0.5.1-rc.1
## 更新内容
* feat: add HarmonyOS NEXT support for react-native-marquee 由 @wangyue6 贡献 [69e384ec36dae1c231a2994589d23fa8589ed403](https://gitee.com/openharmony-sig/rntpc_react-native-marquee/pulls/4)
* fix: The issue of modifying static scanning 由 @wangyue6 贡献 [044db1ac2a4946d1b3bcb83ffce32c58256d2b2e](https://gitee.com/openharmony-sig/rntpc_react-native-marquee/pulls/5)
* pre-release: @react-native-ohos/react-native-marquee@0.5.1-rc.1 由 @wangyue6 贡献 [1d207bda4f69ed6ba8fe69b09f7cb43b57a31c26](https://gitee.com/openharmony-sig/rntpc_react-native-marquee/pulls/6)
